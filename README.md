# self-signed-certs

The scripts creates an SSL config file on the fly, with options to be changed to suit your environment.



```
#!/bin/bash


[ ! -d certs ] && mkdir certs 

echo -e '[req]
prompt = no
distinguished_name = req_distinguished_name
req_extensions = v3_req
                           
[req_distinguished_name]
C = US
ST = California 
L = Los Angeles
O = Our Company Llc
OU = Org Unit Name
CN = Our Company Llc
#emailAddress = info@example.com
                              
[v3_req]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
                      
[alt_names]
DNS.1 = "10.10.10.20"
DNS.2 = "localhost"' > certs/ssl.conf


openssl genrsa -out certs/server.key 2048 - conf certs/ssl.conf
openssl req -new -out certs/server.csr -key certs/server.key -config certs/ssl.conf
openssl x509 -req -days 3650 -in certs/server.csr -signkey certs/server.key -out certs/server.crt
```

# The output:

```
[root@server02 ~]# sh certs.sh 
Generating RSA private key, 2048 bit long modulus
..................................................................+++
.................+++
e is 65537 (0x10001)
Signature ok
subject=/C=US/ST=California/L=Los Angeles/O=Our Company Llc/OU=Org Unit Name/CN=Our Company Llc
Getting Private key
[root@server02 ~]# 
[root@server02 ~]# ls -l certs
total 16
-rw-r--r--. 1 root root 1281 Mar 27 07:17 server.crt
-rw-r--r--. 1 root root 1147 Mar 27 07:17 server.csr
-rw-r--r--. 1 root root 1679 Mar 27 07:17 server.key
-rw-r--r--. 1 root root  505 Mar 27 07:17 ssl.conf
[root@server02 ~]# 
```




